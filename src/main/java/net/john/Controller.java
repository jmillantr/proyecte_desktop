package net.john;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import net.john.models.Article;
import net.john.models.Post;
import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * Created by john on 5/24/17.
 */
public class Controller {

    /**
     * Inicializa los datos del programa
     */
    public void initialize() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.memestring.com/")
                .build();

        MemeStringService service = retrofit.create(MemeStringService.class);

        // Call<List<Post>> posts = service.listPosts("1");
    }

    /**
     * Muestra un mensaje de error, con el texto especificado
     *
     * @param text Mensaje del error
     */
    private void showErrorDialog(String text) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(null);
        alert.setContentText(text);

        alert.showAndWait();
    }

    /**
     * Muestra mensaje de información
     *
     * @param text
     */
    private void showInformationDialog(String text) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText(null);
        alert.setContentText(text);

        alert.showAndWait();
    }

    /**
     * Muestra la versión actual del programa y los datos correspondiente
     */
    public void showVersion() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Version");
        alert.setHeaderText("MemeString V1.0");
        alert.setContentText(String.format("Autor: %s\nURL: %s",
                "Jonathan Millan", "memestring.com"));

        alert.showAndWait();
    }

    /**
     * Cierra la aplicacion
     *
     * @param event
     */
    public void quit(ActionEvent event) {
        Platform.exit();
    }

}
