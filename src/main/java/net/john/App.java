package net.john;

import com.guigarage.flatterfx.FlatterFX;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by john on 5/24/17.
 */
public class App extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/display.fxml"));
        stage.setMinWidth(600);
        stage.setMinHeight(400);
        stage.setTitle("ProyectDesktop");

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        FlatterFX.style();
    }

    public static void main(String[] args) {
        launch(args);
    }

}