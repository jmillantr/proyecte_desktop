package net.john.models;

/**
 * Created by john on 5/24/17.
 */
public class Post {

    private String owner;
    private String media;
    private String caption;

    public Post(String owner, String media, String caption) {
        this.owner = owner;
        this.media = media;
        this.caption = caption;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }
}
