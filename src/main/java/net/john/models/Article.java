package net.john.models;

import java.util.List;

/**
 * Created by john on 5/24/17.
 */
public class Article {

    private Post last;
    private List<Post> related;

    public Article(Post last, List<Post> related) {
        this.last = last;
        this.related = related;
    }

    public Post getLast() {
        return last;
    }

    public void setLast(Post last) {
        this.last = last;
    }

    public List<Post> getRelated() {
        return related;
    }

    public void setRelated(List<Post> related) {
        this.related = related;
    }
}
