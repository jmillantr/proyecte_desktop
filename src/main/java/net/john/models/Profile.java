package net.john.models;

/**
 * Created by john on 5/24/17.
 */
public class Profile {

    private String avatar;
    private String facebook;
    private String twitter;
    private String pinterest;
    private String dribbble;

    public Profile(String avatar, String facebook, String twitter, String pinterest, String dribbble) {
        this.avatar = avatar;
        this.facebook = facebook;
        this.twitter = twitter;
        this.pinterest = pinterest;
        this.dribbble = dribbble;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getPinterest() {
        return pinterest;
    }

    public void setPinterest(String pinterest) {
        this.pinterest = pinterest;
    }

    public String getDribbble() {
        return dribbble;
    }

    public void setDribbble(String dribbble) {
        this.dribbble = dribbble;
    }
}
