package net.john.models;

/**
 * Created by john on 5/24/17.
 */
public class User {

    private String username;
    private String date_joined;
    private Profile profile;
    private int post_count;

    public User(String username, String date_joined, Profile profile, int post_count) {
        this.username = username;
        this.date_joined = date_joined;
        this.profile = profile;
        this.post_count = post_count;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDate_joined() {
        return date_joined;
    }

    public void setDate_joined(String date_joined) {
        this.date_joined = date_joined;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public int getPost_count() {
        return post_count;
    }

    public void setPost_count(int post_count) {
        this.post_count = post_count;
    }
}
