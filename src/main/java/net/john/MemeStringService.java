package net.john;

import net.john.models.Post;
import net.john.models.User;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

import java.util.List;

/**
 * Created by john on 5/24/17.
 */
public interface MemeStringService {
    @GET("users/{username}")
    Call<User> getUser(@Path("username") String username);

    @GET("articles/{article_pk}/posts")
    Call<List<Post>> listPosts(@Path("article_pk") String article_pk);
}